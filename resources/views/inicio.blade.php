	<!-- heredaremos lo que esta en principal
    y la seccion de lo que esta en contenido -->

    @extends('layouts/principal')
@section('contenido')







<body id="top">
<!-- ################################################################################################ -->




<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="bgded overlay" style="background-image:url('images/demo/backgrounds/01.png');">
  <div id="pageintro" class="hoc clear">
    <!-- ################################################################################################ -->
    <article>
      <h3 class="heading">Escuela Militar de Ingeniería</h3>
      <h3 class="heading">"Mcal. Antonio José de Sucre"</h3>
      <!-- <p>In nibh nullam egestas velit laoreet nullam elementum ipsum pharetra suscipit leo augue pretium felis nisl vitae ipsum curabitur quis libero.</p> -->
      <!-- <footer><a class="btn" href="#">Tristique vehicula</a></footer> -->
    </article>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->

<div class="wrapper row3">
  <main class="hoc container clear">
    <!-- main body -->
    <!-- ################################################################################################ -->
    <section id="introblocks">
      <ul class="nospace group btmspace-80 elements elements-four">
        <li class="one_quarter">
          <article><a href="http://172.16.3.63"><i class="fas fa-hand-rock"></i></a>
            <h6 class="heading">Correspondencia</h6>
            <p>Sistema de correspondencia y trámites.</p>
          </article>
        </li>
        <li class="one_quarter">
          <article><a href="https://emi.edu.bo/jdc/"><i class="fas fa-dove"></i></a>
            <h6 class="heading">JDC</h6>
            <p>Sistema académico del Jefe de Carrera.</p>
          </article>
        </li>
        <li class="one_quarter">
          <article><a href="http://personal.servicios.emi.edu.bo"><i class="fas fa-history"></i></a>
            <h6 class="heading">Recursos Humanos</h6>
            <p>RRHH, POAI, Puestos, Funciones, Procedimientos.</p>
          </article>
        </li>
       
       
       
       
        <li class="one_quarter">
          <article><a href="http://evaluacion.emi.edu.bo"><i class="fas fa-heartbeat"></i></a>
            <h6 class="heading">Evaluación y acreditación</h6>
            <p>Sistema de evaluación y acreditación docentes de grado.</p>
          </article>
        </li>
      </ul>
    </section>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- / main body -->
   
  </main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!--
<div class="bgded overlay light" style="background-image:url('images/demo/backgrounds/01.png');">
  <section id="services" class="hoc container clear">
    <!-- ################################################################################################ -->
    <div class="sectiontitle">
      <h6 class="heading font-x2">Lo Ultimo en Nuestro Blog...</h6>
    </div>
    <ul class="nospace group elements elements-three">
      <li class="one_third">
        <article><a href="#">    <!--    <i class="fas fa-hourglass-half"></i>  -->  <img src="https://scontent.flpb2-2.fna.fbcdn.net/v/t1.0-0/p526x296/123141313_2696849937235467_1078394416580839036_o.jpg?_nc_cat=111&ccb=2&_nc_sid=730e14&_nc_ohc=6z1INPMN390AX9UQmsc&_nc_ht=scontent.flpb2-2.fna&tp=6&oh=84e4644ac8cbd17b26f03a2b0b5b24d3&oe=5FCAAC09" alt="">  </a>
          <h6 class="heading">Ingeniero 4.0</h6>
          <p>
         
          En nuestro mes aniversario, te invitamos a participar de nuestros EMI Webinars Gratuitos.<br>
Webinar: "Ingeniero 4.0"<br>
Fecha: 30 de octubre<br>
Hora: 18:00<br>
Expositor: Ph. D. Richard Robles Rodriguez

         
         </p>
        </article>
      </li>
      <li class="one_third">
        <article><a href="#">           <!-- <i class="fas fa-paw">   </i>-->     <img src="https://scontent.flpb2-2.fna.fbcdn.net/v/t1.0-0/p526x296/123140728_2699235326996928_5662568047815639673_o.jpg?_nc_cat=104&ccb=2&_nc_sid=730e14&_nc_ohc=bLPJ_mJnC2MAX8WHqyp&_nc_ht=scontent.flpb2-2.fna&tp=6&oh=56ad0e68debcbc386cac69b2d1f08b66&oe=5FCB35F8 " alt="">    </a>
          <h6 class="heading">Desarrollo e Integración de las Energías Alternativas</h6>
          <p>

          ¡La Escuela Militar de Ingeniería te invita a capacitarte de forma gratuita con nuestros EMI Webinars! <br>
Webinar: "Desarrollo e Integración de las Energías Alternativas" <br>
Fecha: 4 de noviembre <br>
Hora: 18:00 <br>
Expositor: Ing. Omar Iván Paredes

</p>
        </article>
      </li>
      <li class="one_third">
        <article><a href="#">       <!--<i class="fas fa-sliders-h"></i>-->     <img src="https://scontent.flpb2-2.fna.fbcdn.net/v/t1.0-0/p526x296/123277819_2697335560520238_4303533925267319354_o.jpg?_nc_cat=104&ccb=2&_nc_sid=730e14&_nc_ohc=7Pk3Wi4RWc4AX_RINJV&_nc_ht=scontent.flpb2-2.fna&tp=6&oh=43f11eb59b819d8c40d0318b85456793&oe=5FCB3BF9" alt="">            </a>
          <h6 class="heading"> Preuniversitario EMI</h6>
          <p>
          
          
          Curso Preuniversitario EMI Intensivo:<br>
📘  Prepárate en álgebra, cálculo, física y química<br>
👩‍🏫  Docentes altamente calificados y con amplia experiencia<br>
🏠  ¡Inscríbete sin salir de casa!<br>
⏳  Duración: 4 semanas<br>
🗓  Inicio de clases: 9 de noviembre
          
          </p>
      
      
      
      
        </article>
      </li>
      <li class="one_third">
        <article><a href="#">      <!--<i class="fas fa-tty"></i>-->        <img src="https://scontent.flpb2-2.fna.fbcdn.net/v/t1.0-0/p526x296/123358477_2699258486994612_6250892048839742061_o.jpg?_nc_cat=103&ccb=2&_nc_sid=730e14&_nc_ohc=xmVTQ53SVQwAX8pptVG&_nc_ht=scontent.flpb2-2.fna&tp=6&oh=b88f20a1ce338b3d592f72b44db47323&oe=5FCABC05" alt="">        </a>
          <h6 class="heading">2 de noviembre celebramos</h6>
          <p>Hoy  en Bolivia, la festividad de Todos los Santos. A mediodía, empieza el ritual de despedir a las almas de nuestros difuntos, que deben regresar al mundo subterráneo, esto se acompaña de una comida abundante, porque el muerto necesita mucha energía para su viaje de vuelta.

</p>
        </article>
      </li>
      <li class="one_third">
        <article><a href="#">       <!--<i class="fas fa-table"></i>-->    <img src="https://scontent.flpb2-2.fna.fbcdn.net/v/t1.0-0/p526x296/123328610_2699243703662757_4698805560257287268_o.jpg?_nc_cat=105&ccb=2&_nc_sid=730e14&_nc_ohc=aI40-hDayT4AX_pV4oF&_nc_ht=scontent.flpb2-2.fna&tp=6&oh=e56bd489d83df79fbbab27e79a0dc5fc&oe=5FC85E02" alt="">          </a>
          <h6 class="heading">Neumática y Electroneumática en la Industria</h6>
          <p>¡La Escuela Militar de Ingeniería te invita a capacitarte de forma gratuita con nuestros EMI Webinars!
Webinar: "Neumática y Electroneumática en la Industria" <br>
Fecha: 6 de noviembre <br>
Hora: 18:00 <br>
Expositor: Ing. Pablo Viorel Viorel </p>
        </article>
      </li>
      <li class="one_third">
        <article><a href="#">      <!--<i class="fas fa-hand-holding-usd"></i>-->  <img src="https://scontent.flpb2-1.fna.fbcdn.net/v/t1.0-0/p526x296/123141307_2699241093663018_6503369528162115861_o.jpg?_nc_cat=109&ccb=2&_nc_sid=730e14&_nc_ohc=47sN3i0oh84AX9moXmD&_nc_oc=AQkTgWYzbHQVMLhJzlTaSosMbsZNu2mpaNrx-8Iux8gf-83rqsbLmWKKWScrMJ57tko&_nc_ht=scontent.flpb2-1.fna&tp=6&oh=5aed9bdb325fda3b8156a39cf8d878e2&oe=5FCC0B14" alt="">     </a>
          <h6 class="heading">Seguimiento, Control y Supervisión de proyectos de Agua Potable en el Área Rural</h6>
          <p>

          ¡La Escuela Militar de Ingeniería te invita a capacitarte de forma gratuita con nuestros EMI Webinars! <br>
Webinar: "Seguimiento, Control y Supervisión de proyectos de Agua Potable en el Área Rural" <br>
Fecha: 5 de noviembre <br>
Hora: 18:00 <br>
Expositor: Ing. Lucio Otelio Castro 
</p>
        </article>
      </li>
    </ul>
    <!-- ################################################################################################ -->
  </section>
</div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->


</body>
















<!-- con el stop se detiene la pagina para que este no se vuelva a repetir evitando que se duplique-->
@stop