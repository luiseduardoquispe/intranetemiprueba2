<!-- Footer -->
<div class="bgded overlay row4" style="background-image:url('../images/demo/backgrounds/01.png');">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_quarter first">
      <h1 class="logoname"><a href="../index.html">Contactos<span></span></a></h1>
      <p>EMI Irpavi
Telf: 2775536 - 2799505
Fax: 2773774
EMI Central Av. Arce No. 2642
Telf. 2432266 - 2431641 - 2435285

lapaz@adm.emi.edu.bo<a href="#">&hellip;</a></p>



<div class="sociales">
                        <a class="icon-twitter" href="#"></a>
                        <a class="icon-facebook-official" href="#"></a>
                        <a class="icon-whatsapp" href="#"></a>
                        <a class="icon-google-plus-circle" href="#"></a>
                    </div>
















      <ul class="faico clear">
        <li><a class="faicon-facebook" href="#"><i class="fab fa-facebook"></i></a></li>
        <li><a class="faicon-google-plus" href="#"><i class="fab fa-google-plus-g"></i></a></li>
      
        <li><a class="faicon-twitter" href="#"><i class="fab fa-twitter"></i></a></li>

        <i class="fab fa-facebook-f"></i>

        <i class="fab fa-facebook-f"></i>



      
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="heading">Contactos de nuestras Unidades Academicas</h6>
      <ul class="nospace linklist">
        <li><a href="#">Unidad Académica La Paz - EMI</a></li>
        <li><a href="#">Unidad Académica Cochabamba - EMI</a></li>
        <li><a href="#">Unidad Académica Santa Cruz - EMI</a></li>
        <li><a href="#">Unidad Académica Riberalta - EMI</a></li>
        <li><a href="#">Unidad Académica Tropico - EMI</a></li>
      </ul>
    </div>
 
 <!--
   <div class="one_quarter">
      <h6 class="heading">Tincidunt ullamcorper</h6>
      <ul class="nospace clear latestimg">
        <li><a class="imgover" href="#"><img src="https://scontent.flpb2-1.fna.fbcdn.net/v/t1.0-9/105281364_2587397084847420_820753441320624023_n.jpg?_nc_cat=102&_nc_sid=730e14&_nc_ohc=vFWvjoQUsU8AX8MFwqo&_nc_ht=scontent.flpb2-1.fna&oh=3b809bbfc051a185f1d5d80a2d916256&oe=5FA39E04" alt=""></a></li>
        <li><a class="imgover" href="#"><img src="../images/demo/100x100.png" alt=""></a></li>
        <li><a class="imgover" href="#"><img src="../images/demo/100x100.png" alt=""></a></li>
        <li><a class="imgover" href="#"><img src="../images/demo/100x100.png" alt=""></a></li>
        <li><a class="imgover" href="#"><img src="../images/demo/100x100.png" alt=""></a></li>
        <li><a class="imgover" href="#"><img src="../images/demo/100x100.png" alt=""></a></li>
        <li><a class="imgover" href="#"><img src="../images/demo/100x100.png" alt=""></a></li>
        <li><a class="imgover" href="#"><img src="../images/demo/100x100.png" alt=""></a></li>
        <li><a class="imgover" href="#"><img src="../images/demo/100x100.png" alt=""></a></li>
      </ul>
    </div>-->




    <div class="one_quarter">
      <h6 class="heading">Como Llegar</h6>
      <ul class="nospace linklist">
      <li>
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15299.302567448838!2d-68.0868217!3d-16.5348978!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2a399dfb88c485d6!2sEscuela%20Militar%20de%20Ingenier%C3%ADa%20Irpavi!5e0!3m2!1ses-419!2sbo!4v1602100536820!5m2!1ses-419!2sbo" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>


        </li>

 <!--
        <li>
          <article>
            <p class="nospace btmspace-10"><a href="#">Lacinia donec tortor lectus varius vel egestas a dictum in odio mauris metus.</a></p>
            <time class="block font-xs" datetime="2045-04-06">Friday, 6<sup>th</sup> April 2045</time>
          </article>
        </li>
        <li>
          <article>
            <p class="nospace btmspace-10"><a href="#">Turpis iaculis ac hendrerit vel pretium non magna sed non metus ut at nisi morbi.</a></p>
            <time class="block font-xs" datetime="2045-04-05">Thursday, 5<sup>th</sup> April 2045</time>
          </article>
        </li>

-->




      
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>



<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Emi &copy; 2020 - Todos los Derechos Reservados - <a href="#">Educando</a></p>
   <!--  <p class="fl_right">Template by <a target="_blank" href="https://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>-->
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fas fa-chevron-up"></i></a>