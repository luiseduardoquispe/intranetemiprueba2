<!-- Header -->





<div class="wrapper row1">
  <header id="header" class="hoc clear">
    <div id="logo" class="fl_left">
      <!-- ################################################################################################ -->
     
     <div  class="logoname">

      <h1 ><a href="index.html"><img src="../images/logo_emi.png" width="700" height="700" alt=""></a></h1>
      

     </div>
     <!-- ################################################################################################ -->
    </div>
    <nav id="mainav" class="fl_right">
      <!-- ################################################################################################ -->
      <ul class="clear">
        <li class="active"><a href="index.html">Inicio</a></li>
        <li><a class="drop" href="#">Sistemas administrativos</a>
          <ul>
            <li><a target="_blank" href="http://172.16.3.63">Correspondencia</a></li>
            <li><a target="_blank" href="http://172.16.3.65">Sistema Financiero Contable Integrado</a></li>
            <li><a target="_blank" href="http://172.16.3.62">Almacenes</a></li>
            <li><a target="_blank" href="http://personal.servicios.emi.edu.bo">Recursos Humanos</a></li>
            <li><a target="_blank" href="https://poa.servicios.emi.edu.bo">POA</a></li>
            <li><a target="_blank" href="http://resoluciones.servicios.emi.edu.bo">Resoluciones</a></li>
            <li><a  target="_blank" href="http://172.16.3.92">Molinetes</a></li>
            <li><a target="_blank" href="http://repositorio.servicios.emi.edu.bo">Repositorio</a></li>
            <li><a target="_blank" href="http://eminoticias.servicios.emi.edu.bo">Noticias EMI</a></li>
            <li><a target="_blank" href="http://inscripcion.servicios.emi.edu.bo">Registro y Preinscripciones</a></li>
            <li><a target="_blank" href="http://sipdea.servicios.emi.edu.bo">SIPDEA</a></li>
            <li><a target="_blank" href="http://expociencia.servicios.emi.edu.bo">Expociencia 2020</a></li>
          </ul>
        </li>
        <li><a class="drop" href="#">Sistemas grado</a>
          <ul>
            <li><a href="http://172.16.3.35">SAGA</a></li>
            <li><a href="https://www.emi.edu.bo/jdc">JDC</a></li>
              <li><a href="https://www.emi.edu.bo/dtc">DTC</a></li>
            <li><a href="https://notas.emi.edu.bo">Estudiantes notas</a></li>
            <li><a target="_blank" href="http://docentes.servicios.emi.edu.bo">Docentes Grado</a></li>
            <li><a target="_blank" href="http://registro.emi.edu.bo">Docentes Registro de Notas</a></li>
              <li><a target="_blank" href="http://registro.emi.edu.bo">Ingles Registro de Notas</a></li>
              <li><a target="_blank" href="http://evaluacion.emi.edu.bo">Docentes Evaluacion</a></li>
              <li><a target="_blank" href="http://categoriacion.emi.edu.bo">Docentes Evaluacion 2020</a></li>
          </ul>
        </li>
        <li><a class="drop" href="#">Sistemas posgrado</a>
          <ul>
            <li><a href="http://172.16.3.35">SAGA</a></li>
            <li><a href="http://jdcposgrado.emi.edu.bo">JDC</a></li>
            <li><a href="http://docentes.servicios.emi.edu.bo">Docentes posgrado</a></li>
            <li><a href="https://cursos.emi.edu.bo">Estudiantes posgrado notas</a></li
                <li><a target="_blank" href="http://registro.emi.edu.bo">Docentes Registro de Notas</a></li>
          </ul>
        </li>
        <li><a class="drop" href="#">Moodle</a>
          <ul>
            <li><a target="_blank" href="http://campus.lapaz.emi.edu.bo">Grado La Paz</a></li>
            <li><a target="_blank" href="http://campus.cochabamba.emi.edu.bo">Grado Cochabamba</a></li>
            <li><a target="_blank" href="http://campus.santacruz.emi.edu.bo">Grado Santa Cruz</a></li>
            <li><a target="_blank" href="http://campus.riberalta.emi.edu.bo">Grado Riberalta</a></li>
            <li><a target="_blank" href="http://campus.tropico.emi.edu.bo">Grado Trópico</a></li>
            <li><a target="_blank" href="http://posgrado.lapaz.emi.edu.bo">Posgrado La Paz</a></li>
            <li><a target="_blank" href="http://posgrado.cochabamba.emi.edu.bo">Posgrado Cochabamba</a></li>
            <li><a target="_blank" href="http://posgrado.santacruz.emi.edu.bo">Posgrado Santa Cruz</a></li>
            <li><a target="_blank" href="http://posgrado.riberalta.emi.edu.bo">Posgrado Riberalta</a></li>
            <li><a target="_blank" href="http://posgrado.tropico.emi.edu.bo">Posgrado Trópico</a></li>
          </ul>

        </li>
       
           <li><a class="drop" href="#">OPERACIONES</a>
          <ul>
            <li><a href="http://inteligencia.servicios.emi.edu.bo">Inteligencia</a></li>
           
            <li><a href="http://mariscal.servicios.emi.edu.bo">Juego de guerra</a></li>
            
            <li><a href="http://libretas.servicios.emi.edu.bo">Libretas Militares</a></li>
          </ul>
        
       
       
        <li><a class="drop" href="#">Otros</a>
          <ul>
      
            <li><a href="http://ejercito.servicios.emi.edu.bo+">Personal</a></li>
    
            <li><a href="https://gabinete.servicios.emi.edu.bo">Psicología</a></li>
          
          </ul>
        </li>
      </ul>
      <!-- ################################################################################################ -->
    </nav>
  </header>
</div>








<!-- cuando termine el navbar debe mostrar el contenido -->
@yield('contenido')